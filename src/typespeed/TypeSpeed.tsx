import React, { useState, useEffect, useCallback, ChangeEvent, ClipboardEvent, DragEvent } from 'react';
import randomWords from 'random-words';

const randomWord = () => randomWords();

const TIMER = 500;

export const TypeSpeed: React.FC = () => {
    const [word, setWord] = useState(randomWord);
    const [guess, setGuess] = useState('');
    const [score, setScore] = useState(0);
    const [high, setHigh] = useState(0);
    const [lost, setLost] = useState(false);

    useEffect(() => {
        if (!lost && guess === word) {
            setScore(score + 1);
            setGuess('');
            setWord(randomWord);
        }
    }, [score, word, guess, lost]);

    useEffect(() => {
        if (score > high) {
            setHigh(score);
        }
    }, [score, high]);

   

    const changeGuess = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setGuess(event.target.value);
    }, []);

    const handlerCopy = (e: ClipboardEvent) => {
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
    };

    const handlerDrag = (e: DragEvent) => {
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
    };

    return (
        <div className="container mt-3">
            <div>
                <h3>
                    Score: {score} High: {high}
                </h3>
            </div>
            <div className="typespeed" onCopy={handlerCopy} onDragStart={handlerDrag}>
                {word}
            </div>
            <input type="text" value={guess} onChange={changeGuess} />
            {lost ? <div className="typespeed__lost">You've lost!</div> : null}
        </div>
    );
};
