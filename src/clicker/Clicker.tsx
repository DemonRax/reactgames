import React, { useState, useEffect, useCallback } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Shop } from './Shop';

const SPEED = 10

export const Clicker: React.FC = () => {
    const [balance, setBalance] = useState(0);
    const [profit, setProfit] = useState(1);
    const [ct, setCt] = useState(0);

    useEffect(() => {
        let timeout: ReturnType<typeof setTimeout>;
        const startTimer = () => {
            timeout = setTimeout(() => setBalance(balance + profit / SPEED), 1000 / SPEED);
        };
        startTimer();
        return () => {
            clearTimeout(timeout);
        };
    }, [profit, balance]);

    const buy = useCallback(
        (price: number, bonus: number) => () => {
            if (price <= balance) {
                setBalance(balance - price);
                setProfit(profit + bonus);
            }
        },
        [balance, profit]
    );

    const changeCt = useCallback(()=>{
        setCt( ct === 0 ? 2:0 );
    }, [ct])
    

    return (
        <div className="clicker">
            <Container>
                <Row>
                    <Col className="mt-8 clicker__title">Clicker</Col>
                    <Col className="clicker__rechts">Made by RinRax</Col>
                </Row>
                <Row>
                    <Col><div onClick = {changeCt}>{Number(balance).toFixed(ct)} €</div></Col>
                    <Col><div onClick = {changeCt}>+ {Number(profit).toFixed(ct)} €/s</div></Col>
                </Row>
                <Row>
                    <Col>
                        <Shop buy={buy} balance={balance} />
                    </Col>
                </Row>
            </Container>
        </div>
    );
};
