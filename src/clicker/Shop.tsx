import React from 'react';

type ShopProps = {
    buy: (price: number, profit: number) => () => void;
    balance: number;
};

const buttons = [1, 11, 111, 1111, 11111, 111111, 1111111, 11111111, 111111111];

export const Shop: React.FC<ShopProps> = ({ buy, balance }) => {
    return (
        <div className="clicker__shop">
            {buttons.map((profit) => {
                const price = profit * 10 + 1;
                const style = price > balance ? 'nope' : 'yes';
                return (
                    <div
                        key={profit}
                        onClick={buy(price, profit)}
                        className={'clicker__button clicker__button--' + style}>
                        {price} € + {profit} €/s
                    </div>
                );
            })}
        </div>
    );
};
