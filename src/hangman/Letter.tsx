import React from 'react';

type LetterProps = {
    letter: string;
    found: string;
    onClick: () => void;
};

export const Letter: React.FC<LetterProps> = ({ letter, found, onClick }) => (
    <ul className={'hangman__circle ' + found} onClick={onClick}>
        {letter}
    </ul>
);
