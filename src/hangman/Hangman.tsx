import React, { useCallback, useEffect, useState, useRef } from 'react';
import randomWords from 'random-words';
import { Letters } from './Letters';

const ATTEMPTS = 10;

const generateWord = () => randomWords().toUpperCase();

const hangmanPicture = (count: number) => '/hangman/Hangman-' + Math.min(Math.max(0, count), 9).toString() + '.png';

const showWord = (originalWord: string, letters: string[]) =>
    originalWord
        .split('')
        .map((c) => (letters.includes(c) ? c : '_'))
        .join('');

export const Hangman: React.FC = () => {
    const [originalWord, setOriginalWord] = useState(generateWord);
    const [word, setWord] = useState('');
    const [count, setCount] = useState(0);
    const [letters, setLetters] = useState<string[]>([]);

    const newGame = () => {
        const newWord = generateWord();
        setOriginalWord(newWord);
        setLetters([]);
        setCount(0);
        setWord(showWord(newWord, []));
    };

    const newGameRef = useRef(newGame);
    newGameRef.current = newGame;

    useEffect(() => {
        newGameRef.current();
    }, []);

    const won = useCallback(() => word === originalWord, [word, originalWord]);

    const lost = useCallback(() => count >= ATTEMPTS, [count]);

    const playing = useCallback(() => !lost() && !won(), [lost, won]);

    const gameOver = useCallback(() => {
        if (won()) {
            return <p className="hangman__gameOver hangman__gameOver--won">You won!</p>;
        }
        return <p className="hangman__gameOver hangman__gameOver--lost">You lost!</p>;
    }, [won]);

    useEffect(() => {
        setWord(showWord(originalWord, letters));
    }, [originalWord, letters]);

    const onClick = useCallback(
        (letter: string) => () => {
            setCount((count) => count + 1);
            if (!letters.includes(letter)) {
                const newLetters = letters.concat(letter);
                setLetters(newLetters);
            }
        },
        [letters]
    );

    return (
        <div className="container mt-3">
            <div className="row">
                <div className="col-4" />
                <div className="col-8">
                    <h3>Hangman</h3>
                    <p>Classic Hangman game, pick letters to guess the word.</p>
                    <button onClick={newGameRef.current}>Start new</button>
                </div>
            </div>
            <div className="row">
                <div className="col-4">
                    <img src={hangmanPicture(count)} alt={count.toString()} width={'220px'} className="float-right" />
                </div>
                <div className="col-8">
                    <div className="hangman__word">{lost() ? originalWord : word}</div>
                    {playing() ? (
                        <div>
                            <Letters letters={letters} word={originalWord} onClick={onClick} />
                        </div>
                    ) : (
                        gameOver()
                    )}
                </div>
            </div>
        </div>
    );
};
