import React from 'react';
import '../styles/_hangman.scss';
import { Letter } from './Letter';

type LettersProps = {
    letters: string[];
    word: string;
    onClick: (a: string) => () => void;
};

export const Letters: React.FC<LettersProps> = ({ letters, word, onClick }) => {
    const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return (
        <ul className="hangman__letters">
            {Array.from(ALPHABET).map((letter) => {
                let style = 'hangman--none';
                let click = onClick(letter);
                if (letters.includes(letter)) {
                    style = 'hangman--wrong';
                    if (findLetter(word, letter)) {
                        style = 'hangman--right';
                    }
                    click = () => null;
                }
                return <Letter found={style} key={letter} letter={letter} onClick={click} />;
            })}
        </ul>
    );
};

export const findLetter = (word: string, letter: string): boolean => {
    for (let i = 0; i < word.length; i++) {
        if (word.charAt(i) === letter) {
            return true;
        }
    }
    return false;
};
