import React, { useState, useEffect } from 'react';

type BoardProps = {
    state: number[];
    current: number;
    createOnClick: (i: number) => () => void;
    result: (res: number | null) => void;
};

export const Board: React.FC<BoardProps> = ({ state, current, createOnClick, result }) => {
    const [allowed, setAllowed] = useState<number[]>([]);

    useEffect(() => {
        if (current < 0) {
            setAllowed(state.map((v, i) => i));
            return;
        }
        const potentiallyAllowed = [
            current + 21,
            current + 19,
            current + 8,
            current - 12,
            current - 21,
            current - 19,
            current - 8,
            current + 12
        ];
        const all = potentiallyAllowed.filter((i) => {
            if (state[i] !== null) return false;
            return Math.abs((i % 10) - (current % 10)) < 3;
        });
        setAllowed(all);
    }, [state, current, setAllowed]);

    useEffect(() => {
        result(allowed.length === 0 ? state[current] : null);
    }, [allowed, state, current, result]);

    return (
        <div className="horse__board">
            {state.map((v, i) => {
                let allowedClass = '';
                let missingClass = '';
                // let selected = '';
                let onClick = undefined; // now allowed to click

                if (i === current) {
                    // selected = ;
                } else if (allowed.includes(i)) {
                    allowedClass = 'horse__square--allowed';
                    onClick = createOnClick(i);
                } else if (allowed.length === 0 && v === null) {
                    missingClass = 'horse__square--missing';
                }

                return (
                    <div
                        className={classNames(
                            'horse__square',
                            i === current && 'horse__square--last',
                            allowedClass,
                            missingClass
                        )}
                        onClick={onClick}
                        key={i}>
                        {v}
                    </div>
                );
            })}
        </div>
    );
};

const classNamesFilter = (arg: any) => !!arg && typeof arg === 'string';
export const classNames = (...classes: any[]): string | undefined =>
    classes.filter(classNamesFilter).join(' ') || undefined;
