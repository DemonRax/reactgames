import React, { useCallback, useState, useRef, useEffect } from 'react';
import { Board } from './Board';

export const Horseman: React.FC = () => {
    const [gameState, setGameState] = useState<number[]>(Array(100).fill(null));
    const [last, setLast] = useState(-1);
    const [count, setCount] = useState(0);
    const [result, setResult] = useState<number | null>(null);

    const newGame = () => {
        setGameState(Array(100).fill(null));
        setLast(-1);
        setCount(0);
    };

    const newGameRef = useRef(newGame);
    newGameRef.current = newGame;

    useEffect(() => {
        newGameRef.current();
    }, []);

    const onClick = useCallback(
        (i: number) => () => {
            setGameState(
                gameState.map((v, j) => {
                    if (j === i) {
                        return count;
                    }
                    return v;
                })
            );
            setCount((c) => c + 1);
            setLast(i);
        },
        [count, gameState, setCount]
    );

    const gameOver = useCallback(
        (i: number | null) => {
            setResult(i);
        },
        [setResult]
    );

    return (
        <div className="container mt-3">
            <h3>Horseman</h3>
            <p>Cover all cells using chess horse move only.</p>
            <button onClick={newGameRef.current}>Restart</button>
            <Board state={gameState} current={last} createOnClick={onClick} result={gameOver} />
            {result === null ? (
                ''
            ) : result === 99 ? (
                <div className="horse__msg horse__msg--won">You did it! You filled all the cells, congratulations!</div>
            ) : (
                <div className="horse__msg horse__msg--lost">
                    Not bad, you reached the number <b>{result}</b>, but you can do better...
                </div>
            )}
        </div>
    );
};
