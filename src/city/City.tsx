import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Header } from './Header';
import { Field } from './Field';
import { Menu } from './Menu';

const width = 100;
const height = 60;

const generateGame = () => {
    return Array(height + 2).fill(Array(width + 2).fill(0));
};

export const City: React.FC = () => {
    const [field] = useState<number[][]>(generateGame);

    return (
        <Container fluid>
            <Row>
                <Col xs={12}>
                    <Header />
                </Col>
            </Row>
            <Row>
                <Col xs={10}>
                    <Field field={field} />
                </Col>
                <Col xs={2}><Menu/></Col>
            </Row>
        </Container>
    );
};
