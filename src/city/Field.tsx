import React from 'react';

type FieldProps = {
    field: number[][];
};

const cellClass = (cell: number, x: number, y: number) => {
    if (x === 0 || y === 0 || x > 100 || y > 60) {
        return "city__cell city__cell--border"
    } 
    switch (cell) {
        case 0:
            return "city__cell city__cell--empty"
        default:
            return "city__cell city__cell--empty"
    }
};

export const Field: React.FC<FieldProps> = ({ field }) => {
    return (
        <table>
            <tbody>
                {field.map((row, j) => {
                    return (
                        <tr key={j}>
                            {row.map((cell, i) => {
                                return <td key={i * j} className={cellClass(cell, i, j)}></td>;
                            })}
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};
