import React from 'react';

type SquareProps = {
    value: string;
    onClick: () => void;
};

export const Square: React.FC<SquareProps> = ({ value, onClick }) => (
    <button className="ttt__square" onClick={onClick}>
        {value}
    </button>
);
