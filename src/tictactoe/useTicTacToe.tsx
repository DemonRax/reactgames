import { useCallback, useEffect, useRef, useState } from 'react';

type useTicTacToeReturn = {
    newGame: () => void;
    winner: string | null;
    xIsNext: boolean;
    gameState: string[];
    onClick: (i: number) => () => void;
};

export const useTicTacToe = (): useTicTacToeReturn => {
    const [gameState, setGameState] = useState<string[]>(Array(9).fill(null));
    const [winner, setWinner] = useState<string | null>(null);
    const [xIsNext, setNext] = useState(true);

    const newGame = () => {
        setNext(true);
        setGameState(Array(9).fill(null));
        setWinner(null);
    };

    const newGameRef = useRef(newGame);
    newGameRef.current = newGame;

    useEffect(() => {
        newGameRef.current();
    }, []);

    useEffect(() => {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (gameState[a] && gameState[a] === gameState[b] && gameState[a] === gameState[c]) {
                setWinner(gameState[a]);
                return;
            }
        }
        setWinner(null);
    }, [gameState, setWinner]);

    const onClick = useCallback(
        (i: number) => () => {
            if (winner === null && gameState[i] === null) {
                setGameState(
                    gameState.map((v, j) => {
                        if (j === i) {
                            return xIsNext ? 'X' : 'O';
                        }
                        return v;
                    })
                );
                setNext(!xIsNext);
            }
        },
        [winner, xIsNext, gameState, setGameState, setNext]
    );

    return { newGame, winner, xIsNext, gameState, onClick };
};
