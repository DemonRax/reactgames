import React from 'react';
import { Board } from './Board';
import { useTicTacToe } from './useTicTacToe';

export const TicTacToe: React.FC = () => {
    const { newGame, winner, xIsNext, gameState, onClick } = useTicTacToe();

    return (
        <div className="container mt-3">
            <div className="col-8">
                <h3>Tic Tac Toe</h3>
                <p>
                    Classic Tic Tac Toe game for two players (only local multiplayer supported). Click to set X's and
                    O's and see who wins the battle.
                </p>
                <button onClick={newGame}>Restart</button>
            </div>
            <div className="ttt__status">
                {winner ? (
                    <div data-testid="status" className="ttt--won">
                        And the winner is... player {winner}!
                    </div>
                ) : (
                    <div data-testid="status">Next player: {xIsNext ? 'X' : 'O'}</div>
                )}
            </div>
            <Board state={gameState} onClick={onClick} />
        </div>
    );
};
