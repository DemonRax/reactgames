import React from 'react';
import { Square } from './Square';

type BoardProps = {
    state: string[];
    onClick: (a: number) => () => void;
};

export const Board: React.FC<BoardProps> = ({ state, onClick }) => (
    <div>
        <div className="ttt__board-row">
            <Square value={state[0]} onClick={onClick(0)} />
            <Square value={state[1]} onClick={onClick(1)} />
            <Square value={state[2]} onClick={onClick(2)} />
        </div>
        <div className="ttt__board-row">
            <Square value={state[3]} onClick={onClick(3)} />
            <Square value={state[4]} onClick={onClick(4)} />
            <Square value={state[5]} onClick={onClick(5)} />
        </div>
        <div className="ttt__board-row">
            <Square value={state[6]} onClick={onClick(6)} />
            <Square value={state[7]} onClick={onClick(7)} />
            <Square value={state[8]} onClick={onClick(8)} />
        </div>
    </div>
);
