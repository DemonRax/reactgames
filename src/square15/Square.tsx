import React from 'react';

type SquareProps = {
    value: string;
    onClick: () => void;
};

export const Square: React.FC<SquareProps> = ({ value, onClick }) => {
    const empty = value === '0' ? 'sq15__square--empty' : '';
    return (
        <button className={'sq15__square ' + empty} onClick={onClick}>
            {value}
        </button>
    );
};
