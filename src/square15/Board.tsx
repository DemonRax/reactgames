import React from 'react';
import { Square } from './Square';

type BoardProps = {
    state: number[];
    onClick: (a: number) => () => void;
};

export const Board: React.FC<BoardProps> = ({ state, onClick }) => (
    <div className="sq15__board">
        {state.map((v, i) => (
            <Square key={i} value={v.toString()} onClick={onClick(i)} />
        ))}
    </div>
);
