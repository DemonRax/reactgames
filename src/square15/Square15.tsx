import React, { useState, useRef, useEffect, useCallback } from 'react';
import { Board } from './Board';

const INIT_ARRAY = Array.from([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0]);

const shuffleN = (array: number[]): number[] => {
    for (let i = 0; i < 1000; i++) {
        const k = Math.floor(Math.random() * 16);
        const z = array.findIndex((x) => x === 0);
        if (canSwap(k, z)) {
            swap(array, k, z);
        }
    }
    return Array.from(array);
};

const plus = [3, 7, 11];
const minus = [4, 8, 12];

const canSwap = (i: number, j: number): boolean => {
    if (i < 0 && j > 15) {
        return false;
    }
    if (plus.includes(i) && j === i + 1) {
        return false;
    }
    if (minus.includes(i) && j === i - 1) {
        return false;
    }
    return j === i + 1 || j === i - 1 || j === i + 4 || j === i - 4;
};

const swap = (array: number[], i: number, j: number): number[] => {
    if (array[i] === 0 || array[j] === 0) {
        [array[i], array[j]] = [array[j], array[i]];
    }
    return Array.from(array);
};

export const Square15: React.FC = () => {
    const [game, setGame] = useState<number[]>(shuffleN(Array.from(INIT_ARRAY)));
    const [zeroIndex, setZeroIndex] = useState(0);

    const newGame = () => {
        const array = shuffleN(Array.from(INIT_ARRAY));
        setGame(array);
        setZeroIndex(array.findIndex((x) => x === 0));
    };

    const newGameRef = useRef(newGame);
    newGameRef.current = newGame;

    useEffect(() => {
        newGameRef.current();
    }, []);

    const won = useCallback(() => JSON.stringify(game) === JSON.stringify(INIT_ARRAY), [game]);

    const onClick = useCallback(
        (i: number) => () => {
            if (won()) {
                return () => undefined;
            }
            if (canSwap(i, zeroIndex)) {
                const array = swap(game, i, zeroIndex);
                setGame(array);
                setZeroIndex(array.findIndex((x) => x === 0));
            }
        },
        [game, zeroIndex, won]
    );

    return (
        <div className="container mt-3">
            <h3>Square</h3>
            <p>Click on tiles to move them to the empty space and order them.</p>
            <button onClick={newGameRef.current}>Restart</button>
            <Board state={game} onClick={onClick} />
            {won() ? <div className="sq15__won">You won! Congratulations!</div> : null}
        </div>
    );
};
