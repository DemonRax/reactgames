import React, { useCallback, useEffect, useState } from 'react';

const width = 120;
const height = 60;

const draw = (x: number, y: number, j: number, i: number): string => {
    if (x === j && y === i) return 'ball__ball';
    return ' ';
};

export const Ball: React.FC = () => {
    const [x, setX] = useState(50);
    const [y, setY] = useState(10);
    const [dx, setDX] = useState(5);
    const [dy, setDY] = useState(8);

    const changeX = useCallback((e: any) => {
        setX(Number(e.target.value));
    }, []);
    const changeY = useCallback((e: any) => {
        setY(Number(e.target.value));
    }, []);

    const move = useCallback(() => {
        const ndx = x <= 0 || x >= width - 1 ? -dx : dx;
        const ndy = y <= 0 || y >= height - 1 ? -dy : dy;
        setDX(ndx);
        setDY(ndy);
        setX((x) => Math.min(Math.max(0, x + ndx), width - 1));
        setY((y) => Math.min(Math.max(0, y + ndy), height - 1));
    }, [x, y, dx, dy]);

    useEffect(() => {
        let timeout: ReturnType<typeof setTimeout>;
        const startTimer = () => {
            timeout = setTimeout(() => move(), 5);
        };
        startTimer();
        return () => {
            clearTimeout(timeout);
        };
    }, [x, y, move]);

    return (
        <div>
            <table>
                <tbody>
                    <tr>
                        {Array(width + 2)
                            .fill(null)
                            .map((_, i) => (
                                <td key={'top' + i} className="ball__cell ball__cell--wall"></td>
                            ))}
                    </tr>

                    {Array(height)
                        .fill(null)
                        .map((_, i) => (
                            <tr key={'row' + i}>
                                <td className="ball__cell ball__cell--wall"></td>
                                {Array(width)
                                    .fill(null)
                                    .map((_, j) => (
                                        <td key={i + j} className={'ball__cell ' + draw(x, y, j, i)}></td>
                                    ))}
                                <td className="ball__cell ball__cell--wall"></td>
                            </tr>
                        ))}

                    <tr>
                        {Array(width + 2)
                            .fill(null)
                            .map((_, i) => (
                                <td key={'bottom' + i} className="ball__cell ball__cell--wall"></td>
                            ))}
                    </tr>
                </tbody>
            </table>
            <input type="number" onChange={changeX} value={x} />
            <input type="number" onChange={changeY} value={y} />
        </div>
    );
};
