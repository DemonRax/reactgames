import React from 'react';
import { useGuess } from './useGuess';

export const Guess: React.FC = () => {
    const { guess, changeGuess, status, newGame } = useGuess();

    return (
        <div className="container mt-3">
            <h3>Guess a number</h3>
            <p>Classic game of guessing a number. Enter a number and follow instructions.</p>
            <div>
                <button onClick={newGame(null)}>Restart</button>
            </div>
            <div className="guess">
                <input type="number" value={guess} onChange={changeGuess} />
            </div>
            <div className={status.class}>{status.msg}</div>
        </div>
    );
};
