import React from 'react';
import { render } from '@testing-library/react';
import { Guess } from './Guess';

describe('Guess', () => {
    test('should match initial snapshot', () => {
        const { getByText } = render(<Guess />);
        expect(getByText).toMatchSnapshot();
    });
});
