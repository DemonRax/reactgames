import { ChangeEvent } from 'react';
import { act, renderHook } from '@testing-library/react-hooks';
import { useGuess } from './useGuess';

describe('use guess hooks', () => {
    test('should init null', () => {
        const { result } = renderHook(useGuess);

        expect(result.current.guess).toBeUndefined();
        expect(result.current.status).toEqual({ class: '', msg: '' });

        act(() => {
            result.current.newGame(null)();
        });
        expect(result.current.guess).toBe(0);
    });

    test('should init with value', () => {
        const { result } = renderHook(useGuess);

        act(() => {
            result.current.newGame(47)();
        });

        act(() => {
            result.current.changeGuess({ target: { value: '50' } } as ChangeEvent<HTMLInputElement>);
        });
        expect(result.current.guess).toBe(50);

        act(() => {
            result.current.changeGuess({ target: { value: 'blah' } } as ChangeEvent<HTMLInputElement>);
        });
        expect(result.current.guess).toBe(50);
    });

    test('should render status high', () => {
        const { result } = renderHook(useGuess);
        act(() => {
            result.current.newGame(47)();
        });
        act(() => {
            result.current.changeGuess({ target: { value: '50' } } as ChangeEvent<HTMLInputElement>);
        });
        expect(result.current.guess).toBe(50);
        expect(result.current.status).toEqual({ class: 'guess__msg', msg: 'The number 50 is too high!' });
    });

    test('should render status low', () => {
        const { result } = renderHook(useGuess);
        act(() => {
            result.current.newGame(47)();
        });
        act(() => {
            result.current.changeGuess({ target: { value: '40' } } as ChangeEvent<HTMLInputElement>);
        });
        expect(result.current.guess).toBe(40);
        expect(result.current.status).toEqual({ class: 'guess__msg', msg: 'The number 40 is too low!' });
    });

    test('should render status won', () => {
        const { result } = renderHook(useGuess);
        act(() => {
            result.current.newGame(47)();
        });
        act(() => {
            result.current.changeGuess({ target: { value: '47' } } as ChangeEvent<HTMLInputElement>);
        });
        expect(result.current.guess).toBe(47);
        expect(result.current.status).toEqual({ class: 'guess__msg guess--won', msg: 'Correct! You won!' });
    });
});
