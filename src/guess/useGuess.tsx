import { ChangeEvent, useCallback, useState } from 'react';

type Status = {
    class: string;
    msg: string;
};

const randomNumber = () => Math.floor(Math.random() * 100) + 2;

export type useGuessReturn = {
    guess: number | undefined;
    changeGuess: (event: ChangeEvent<HTMLInputElement>) => void;
    status: Status;
    newGame: (n: number | null) => () => void;
};

export const useGuess = (): useGuessReturn => {
    const [number, setNumber] = useState(randomNumber);
    const [guess, setGuess] = useState<number>();

    const changeGuess = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            const num = Number(event.target.value);
            if (!num) {
                return;
            }
            setGuess(Math.min(Math.max(0, num), 100));
        },
        [setGuess]
    );

    const renderStatus = useCallback((): Status => {
        if (!guess) {
            return { class: '', msg: '' };
        }
        if (number < guess) {
            return { class: 'guess__msg', msg: 'The number ' + guess + ' is too high!' };
        }
        if (number > guess) {
            return { class: 'guess__msg', msg: 'The number ' + guess + ' is too low!' };
        }
        return { class: 'guess__msg guess--won', msg: 'Correct! You won!' };
    }, [number, guess]);

    const newGame = (n: number | null) => () => {
        if (!n) {
            n = randomNumber();
        }
        setNumber(n);
        setGuess(0);
    };

    return { guess, changeGuess, status: renderStatus(), newGame };
};
