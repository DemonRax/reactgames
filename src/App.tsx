import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import { Empty } from './Empty';
import { Guess } from './guess/Guess';
import { Hangman } from './hangman/Hangman';
import { TicTacToe } from './tictactoe/TicTacToe';
import { Horseman } from './horseman/Horseman';
import { Square15 } from './square15/Square15';
import { TypeSpeed } from './typespeed/TypeSpeed';
import { Ball } from './ball/Ball';
import { City } from './city/City';
import { Clicker } from './clicker/Clicker';

export const App: React.FC = () => (
    <div className="App">
        <nav className="navbar bg-light navbar-expand-sm navbar-light">
            <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                    <Link to="/guess" className="nav-link">
                        Guess Number
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/hangman" className="nav-link">
                        Hangman
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/tictactoe" className="nav-link">
                        TicTacToe
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/horseman" className="nav-link">
                        Horseman
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/square" className="nav-link">
                        Square
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/typespeed" className="nav-link">
                        TypeSpeed
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/ball" className="nav-link">
                        Ball Game
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/clicker" className="nav-link">
                        Clicker
                    </Link>
                </li>
                <li className="navbar-item">
                    <Link to="/city" className="nav-link">
                        City
                    </Link>
                </li>
            </ul>
        </nav>
        <Switch>
            <Route exact path="/" component={Empty} />
            <Route path="/guess" component={Guess} />
            <Route path="/hangman" component={Hangman} />
            <Route path="/tictactoe" component={TicTacToe} />
            <Route path="/horseman" component={Horseman} />
            <Route path="/square" component={Square15} />
            <Route path="/typespeed" component={TypeSpeed} />
            <Route path="/ball" component={Ball} />
            <Route path="/city" component={City} />
            <Route path="/clicker" component={Clicker} />
        </Switch>
    </div>
);
